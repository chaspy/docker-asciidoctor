# docker-asciidoctor
test project for asciidoc build by gitlab-ci

## Reference
* [asciidoctor/docker-asciidoctor](https://github.com/asciidoctor/docker-asciidoctor)
* [脱Word、脱Markdown、asciidocでドキュメント作成する際のアレコレ](https://qiita.com/tamikura@github/items/5d3f62dae55617ee42bb)
